package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_a_paking_ticket_when_park_given_a_car(){
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(1);
        //when
       ParkingTiket parkingTiket = parkingLot.park(car);
       //then
        Assertions.assertNotNull(parkingTiket);
    }
    @Test
    void should_return_a_car_when_fetch_given_a_ticket(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingTiket parkingTiket = parkingLot.park(car);
        Car car1 = parkingLot.fetch(parkingTiket);
        Assertions.assertEquals(car1,car);
    }
    @Test
    void should_return_two_car_when_fetch_given_two_ticket(){
        Car car = new Car();
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingTiket parkingTiket = parkingLot.park(car);
        Car car2 = parkingLot.fetch(parkingTiket);
        ParkingTiket parkingTiket1 = parkingLot.park(car1);
        Car car3 = parkingLot.fetch(parkingTiket1);
        Assertions.assertEquals(car2,car);
        Assertions.assertEquals(car3,car1);
    }
    @Test
    void should_return_error_when_fetch_given_a_invalid_ticket(){
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingTiket parkingTiket = new ParkingTiket();
       // Car car1 = pakingLot.fetch(parkingTiket);
        Assertions.assertThrows(UnrecognizedParkingException.class,()-> parkingLot.fetch(null));

    }
    @Test
    void should_return_error_when_fetch_given_a_used_ticket(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingTiket parkingTiket = parkingLot.park(car);
        Car car1 = parkingLot.fetch(parkingTiket);
        Assertions.assertThrows(UnrecognizedParkingException.class,()-> parkingLot.fetch(parkingTiket));
    }

    @Test
    void should_return_error_when_park_given_a_car_and_parking_is_full(){
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(1);
        //when
        ParkingTiket parkingTiket = parkingLot.park(car);
        //then
        Assertions.assertThrows(FullPositionException.class,()-> parkingLot.park(car));
    }


}
