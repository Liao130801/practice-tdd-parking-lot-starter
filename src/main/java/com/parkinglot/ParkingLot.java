package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private Map<ParkingTiket, Car> carMap = new HashMap<>();
    private int parkingSpaces;
    protected int currentParkingSpaces;


    private ParkingBoy parkingBoy;


    private ParkingLotManager parkingLotManager;

    public ParkingLot(int parkingSpaces) {
        this.parkingSpaces = parkingSpaces;
        this.currentParkingSpaces = parkingSpaces;
    }

    public ParkingBoy getParkingBoy() {
        return parkingBoy;
    }

    public ParkingLotManager getParkingLotManager() {
        return parkingLotManager;
    }

    public void attachedParkingBoy(ParkingBoy parkingBoy) {
        this.parkingBoy = parkingBoy;
    }

    public void attachedParkingLotManager(ParkingLotManager parkingLotManager) {
        this.parkingLotManager = parkingLotManager;
    }

    public ParkingTiket park(Car car) {
        if (currentParkingSpaces <= 0) {
            throw new FullPositionException("No Avalible Position");
        }
        ;
        ParkingTiket parkingTiket = new ParkingTiket();
        carMap.put(parkingTiket, car);
        currentParkingSpaces--;
        return parkingTiket;
    }

    public Car fetch(ParkingTiket parkingTiket) {
        Car car = carMap.get(parkingTiket);
        if (parkingTiket == null || car == null) {
            throw new UnrecognizedParkingException("Unrecognized parking ticket.");
        }
        carMap.remove(parkingTiket);
        if (currentParkingSpaces < parkingSpaces) {
            currentParkingSpaces++;
        }
        return car;
    }

    public Boolean isFull() {
        System.out.println(carMap.size() >= parkingSpaces);
        return carMap.size() > 0 && carMap.size() >= parkingSpaces;
    }

    public int getRemainParkingSpaces() {
        return parkingSpaces - carMap.size();
    }

    public double calculateVacancyRate() {
        return parkingSpaces - carMap.size() / parkingSpaces;
    }
}
