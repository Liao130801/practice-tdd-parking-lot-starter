package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoyTest {
    @Test
    void should_return_a_paking_ticket_when_park_given_a_car_and_a_parkingboy() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(7);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        parkingLot2.park(car);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        //when
        ParkingTiket parkingTiket = parkingBoy.park(car);
        //then
        Assertions.assertEquals(parkingLot2,parkingTiket.getParkingLot());
    }

    @Test
    void should_return_car_when_fetch_given_a_paking_ticket_and_a_parkingboy() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(10));
        parkingLotList.add(new ParkingLot(10));
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        ParkingTiket parkingTiket = parkingBoy.park(car);
        //when
        Car car1 = parkingBoy.fetch(parkingTiket);
        //then
        Assertions.assertEquals(car1, car);
    }

    @Test
    void should_return_two_car_when_fetch_given_two_ticket_and_a_parkingboy() {
        Car car = new Car();
        Car car1 = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(10));
        parkingLotList.add(new ParkingLot(10));
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        ParkingTiket parkingTiket = parkingBoy.park(car);
        Car car2 = parkingBoy.fetch(parkingTiket);
        ParkingTiket parkingTiket1 = parkingBoy.park(car1);
        Car car3 = parkingBoy.fetch(parkingTiket1);
        Assertions.assertEquals(car2, car);
        Assertions.assertEquals(car3, car1);
    }

    @Test
    void should_return_error_when_fetch_given_a_invalid_ticket_and_a_parkingboy() {
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(10));
        parkingLotList.add(new ParkingLot(10));
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        ParkingTiket parkingTiket = new ParkingTiket();
        // Car car1 = pakingLot.fetch(parkingTiket);
        Assertions.assertThrows(UnrecognizedParkingException.class, () -> parkingBoy.fetch(null));

    }

    @Test
    void should_return_error_when_fetch_given_a_used_ticket_and_a_parkingboy() {
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(10));
        parkingLotList.add(new ParkingLot(10));
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        ParkingTiket parkingTiket = parkingBoy.park(car);
        Car car1 = parkingBoy.fetch(parkingTiket);
        Assertions.assertThrows(UnrecognizedParkingException.class, () -> parkingBoy.fetch(parkingTiket));
    }

    @Test
    void should_return_error_when_park_given_a_car_and_parking_is_full() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(new ParkingLot(1));
        parkingLotList.add(new ParkingLot(1));
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        //when
        ParkingTiket parkingTiket = parkingBoy.park(car);
        ParkingTiket parkingTiket1 = parkingBoy.park(car);
        //then
        Assertions.assertThrows(FullPositionException.class, () -> parkingBoy.park(car));
    }

}
