package com.parkinglot;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ParkingBoy {
    private ParkingLot parkingLot;
    private List<ParkingLot> parkingLotList;

    public ParkingBoy(List<ParkingLot> parkingLotList) {
        this.parkingLotList = parkingLotList;

    }

    public ParkingTiket park(Car car) {
        List<ParkingLot> notFullParkingLotList = parkingLotList.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .collect(Collectors.toList());
        if (notFullParkingLotList.size() == 0) {
            throw new FullPositionException("No Available Position");
        }
        ParkingLot hasMostEmptyParkingLot = notFullParkingLotList.stream()
                .max(Comparator.comparingDouble(ParkingLot::calculateVacancyRate))
                .orElse(null);
        ParkingTiket parkingTiket = hasMostEmptyParkingLot.park(car);
        parkingTiket.attachedParkingLot(hasMostEmptyParkingLot);
        hasMostEmptyParkingLot.attachedParkingBoy(this);
        return parkingTiket;
    }

    public Car fetch(ParkingTiket parkingTiket) {
        if (parkingTiket == null) {
            throw new UnrecognizedParkingException("Unrecognized parking ticket.");
        }
        Car car = parkingTiket.getParkingLot().fetch(parkingTiket);
        return car;
    }

    public Boolean hasNoAvailableParkingLot() {
        List<ParkingLot> notFullParkingLotList = parkingLotList.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .collect(Collectors.toList());
        return notFullParkingLotList.size() == 0;
    }
    public double calculateVacancyRate(){
        ParkingLot hasMostEmptyParkingLot = parkingLotList.stream()
                .max(Comparator.comparingDouble(ParkingLot::calculateVacancyRate))
                .orElse(null);
        return hasMostEmptyParkingLot.calculateVacancyRate();
    }
}
