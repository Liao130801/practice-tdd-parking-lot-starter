package com.parkinglot;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingLotManager {
    private List<ParkingBoy> parkingBoyList;
    private List<ParkingLot> parkingLotList;
    public ParkingLotManager(List<ParkingBoy> parkingBoyList) {
        this.parkingBoyList = parkingBoyList;
    }
    public void saveParkingLotList(List<ParkingLot> parkingLotList) {
        this.parkingLotList = parkingLotList;
    }
    public ParkingTiket park(Car car) {
        List<ParkingLot> notFullParkingLotList = parkingLotList.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .collect(Collectors.toList());
        if (notFullParkingLotList.size() == 0) {
            throw new FullPositionException("No Available Position");
        }
        ParkingLot hasMostEmptyParkingLot = notFullParkingLotList.stream()
                .max(Comparator.comparingDouble(ParkingLot::calculateVacancyRate))
                .orElse(null);
        ParkingTiket parkingTiket = hasMostEmptyParkingLot.park(car);
        parkingTiket.attachedParkingLot(hasMostEmptyParkingLot);
        hasMostEmptyParkingLot.attachedParkingLotManager(this);
        return parkingTiket;
    }

    public Car fetch(ParkingTiket parkingTiket) {
        if (parkingTiket == null) {
            throw new UnrecognizedParkingException("Unrecognized parking ticket.");
        }
        Car car = parkingTiket.getParkingLot().fetch(parkingTiket);
        return car;
    }
    public ParkingTiket specifyParkingBoyToPark(Car car){
        List<ParkingBoy> availableParkingBoyList = parkingBoyList.stream()
                .filter(parkingBoyInList -> !parkingBoyInList.hasNoAvailableParkingLot())
                .collect(Collectors.toList());
        if (availableParkingBoyList.size() == 0) {
            throw new FullPositionException("No Available Position");
        }
        ParkingBoy parkingBoy = parkingBoyList.stream()
                .max(Comparator.comparingDouble(ParkingBoy::calculateVacancyRate))
                .orElse(null);
        return parkingBoy.park(car);
    }
    public Car specifyParkingBoyToFetch(ParkingTiket parkingTiket) {
        if (parkingTiket == null) {
            throw new UnrecognizedParkingException("Unrecognized parking ticket.");
        }
        Car car = parkingTiket.getParkingLot().getParkingBoy().fetch(parkingTiket);
        return car;
    }

}
