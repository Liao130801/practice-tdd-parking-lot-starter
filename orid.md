###1.Key learning, skills and experience acquired
- 1). Today we had a code review of the Command Patterns used by our group of students and there was a slight problem with them not really understanding the use of Command Patterns. This problem has brought me a little inspiration, in the future, we need to understand the design pattern more deeply in order to apply in the code, otherwise it may be counterproductive.

- 2) Learned the observer pattern, the observer pattern is a very important design pattern, many open source libraries use this pattern. After learning how to use it, I want to apply more design patterns to my daily development. 
     
###2.Problem / Confusing / Difficulties

- I don't know much about Command Patterns, and I can't tell the difference between Command Patterns and Strategy Patterns, so I'm going to learn more about them and try to apply them to my projects.

###3.Other Comments / Suggestion

- I need to learn more.