package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ParkingLotManagerTest {



    @Test
    void should_return_a_paking_ticket_when_specifyParkingBoyToPark_given_a_car_and_a_parkingboy_and_a_pakingLotManager() {
        //given
        Car car = new Car();
        //create parkingLotList
        List<ParkingLot> parkingLotList = new ArrayList<>();
        List<ParkingLot> parkingLotList1 = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(5);
        ParkingLot parkingLot1 = new ParkingLot(7);
        parkingLot1.park(car);
        parkingLotList.add(parkingLot);
        parkingLotList1.add(parkingLot1);
        //create parkingBoyList
        List<ParkingBoy> parkingBoyList = new ArrayList<>();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        ParkingBoy parkingBoy1 = new ParkingBoy(parkingLotList1);
        parkingBoyList.add(parkingBoy);
        parkingBoyList.add(parkingBoy1);
        //create parkingLotManager
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingBoyList);
        //when
        ParkingTiket parkingTiket = parkingLotManager.specifyParkingBoyToPark(car);
        //then
        Assertions.assertNotNull(parkingTiket);
        Assertions.assertEquals(parkingLot1,parkingTiket.getParkingLot());
    }
    @Test
    void should_return_a_car_when_specifyParkingBoyToFetch_given_a_paking_ticket_and_a_parkingboy_and_a_pakingLotManager() {
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(5);
        parkingLotList.add(parkingLot);
        List<ParkingBoy> parkingBoyList = new ArrayList<>();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        parkingBoyList.add(parkingBoy);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingBoyList);
        ParkingTiket parkingTiket = parkingLotManager.specifyParkingBoyToPark(car);
        Car car1 = parkingLotManager.specifyParkingBoyToFetch(parkingTiket);
        Assertions.assertEquals(car1,car);
    }
    @Test
    void should_return_a_car_when_park_given_a_paking_ticket_and_a_parkingboy_and_a_pakingLotManager() {
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(5);
        parkingLotList.add(parkingLot);
        List<ParkingBoy> parkingBoyList = new ArrayList<>();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        parkingBoyList.add(parkingBoy);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingBoyList);
        parkingLotManager.saveParkingLotList(parkingLotList);
        ParkingTiket parkingTiket = parkingLotManager.park(car);
        Assertions.assertNotNull(parkingTiket);
    }
    @Test
    void should_return_error_when_fetch_given_a_invalid_ticket_and_a_parkingboy_and_a_pakingLotManager() {
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(5);
        parkingLotList.add(parkingLot);
        List<ParkingBoy> parkingBoyList = new ArrayList<>();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        parkingBoyList.add(parkingBoy);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingBoyList);
        ParkingTiket parkingTiket = parkingLotManager.specifyParkingBoyToPark(car);
        Assertions.assertThrows(UnrecognizedParkingException.class, () -> parkingBoy.fetch(null));

    }
    @Test
    void should_return_error_when_fetch_given_a_used_ticket_and_a_parkingboy_and_a_pakingLotManager() {
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(5);
        parkingLotList.add(parkingLot);
        List<ParkingBoy> parkingBoyList = new ArrayList<>();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        parkingBoyList.add(parkingBoy);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingBoyList);
        ParkingTiket parkingTiket = parkingLotManager.specifyParkingBoyToPark(car);
        Car car1 = parkingLotManager.specifyParkingBoyToFetch(parkingTiket);
        Assertions.assertThrows(UnrecognizedParkingException.class, () -> parkingLotManager.specifyParkingBoyToFetch(parkingTiket));
    }
    @Test
    void should_return_error_when_park_given_a_car_and_parking_is_full_and_a_pakingLotManager() {
        Car car = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLotList.add(parkingLot);
        List<ParkingBoy> parkingBoyList = new ArrayList<>();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        parkingBoyList.add(parkingBoy);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingBoyList);
        parkingLotManager.specifyParkingBoyToPark(car);
        //then
        Assertions.assertThrows(FullPositionException.class, () -> parkingLotManager.specifyParkingBoyToPark(car));
    }
}
